# SCG - Synthetic cases generator for sensitivity analyses

This project serves as a framework to simulate and solve synthetic sensitivity tests related to aircraft identification (currently only the 5-dimensional airspeed and wind estimation problem).

The program uses an external parallel PSO solver. The needed executable (oem-pso), can be obtained from the [OEM-CUDA-PSO project](https://bitbucket.org/carlos-rubio/oem-cuda-pso)

## Configuring

First, it is recommended to load the code in an IDE, like IntelliJ-IDEA. In IntelliJ-IDEA this can be done directly from File -> New -> Project from Version Control -> git, and using the URL: git@bitbucket.org:carlos-rubio/scg.git

Once loaded the source code into the IDE, the next step is to configure paths. Open the *Config.ky* class and update these paths:

**outputFolder**: Where the results of the sensitivity analyses will be written  

**oemCudaFolder**: Where it is the oem-pso executable  
**jsbSimFolder**: Where is installed the JSBSim simulator (needed only for the validation section) 

All of them need a folder with write permissions.


## Running

Analyses are in the *net.xnor.scg.experiments* package, one class per sensitivity analysis. Each of these classes has its own main function. Currently, existing classes are:

**C1** - Convergence test  
**S1** to **S4** - Uni-dimensional sensitivity analysis  
**B12** to **B34** - Bi-dimensional sensitivity analysis  
**M1** - Multi-dimensional sensitivity analysis  

The resulting analyses can be compacted (averaging the results of several swarms for the same model parameters), executing the class *net.xnor.scg.util.Compactor*. 

## Validation

To reproduce the results included in the validation section, follow the next steps:  

1- Download and install the JSBSim simulator from [JSBSim project](http://jsbsim.sourceforge.net/)  
2- Copy the files inside the JSBSim folder of the present project in the corresponding folders of the JSBSim simulator to update: the Cessna C310 model, initial conditions, autopilot, engine, propeller XMLs. Also copy the script template ("script_C310_template.xml") into the JSBSim scripts folder  
3- Set the JSBSim path ("jsbSimFolder" property ) in the *Config.ky* class  

Then, execute the validations lanching the classes:

**ValidationCalm** - Create the precision surface executing the configured grid, or execute only one grid point in calm conditions.  
**ValidationTurbulence** - Execute one grid point in turbulence conditions.  

In both cases, remember to adjust the turbulence severity level in the script_C310_template.xml file from 0 to calm or severity 1 or 2 for turbulence conditions.


