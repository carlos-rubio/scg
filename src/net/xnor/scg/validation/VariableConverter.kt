/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.validation

import net.xnor.geodetic.Geodesic
import net.xnor.geodetic.Transforms
import net.xnor.scg.calculators.IASCalculator
import net.xnor.scg.calculators.TASCalculator
import net.xnor.scg.model.Sensor
import net.xnor.scg.model.Variable
import net.xnor.scg.synthetic.Generator
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.util.CSVAdapter
import net.xnor.scg.util.CSVReader
import net.xnor.scg.util.CSVWriter
import java.io.File

/**
 * Convert JSBSim output (csv file) to the list of variables needed by the PSO solver
 * @author Carlos Rubio Sierra
 *
 */
@Suppress("LocalVariableName")
class VariableConverter() {

    fun convert(flightData: File, discardSamples: Int = 0, sensorModel: Sensor) : Generator.VariableHolder {

        // read JSBSim output variables
        val csvReader = CSVReader(flightData)
        val csvAdapter = CSVAdapter(csvReader)

        // Real time
        var realTime = Variable("realTime", "time", "s", Default.frecuency, csvReader.readVariable("Time"))

        // Dynamic pressure
        var dynamicPressure = csvAdapter.read("/fdm/jsbsim/aero/qbar-psf", "q", "real dynamic pressure", "Pa", Default.frecuency) { it * 47.88026 }

        // temperature variable
        var OAT = csvAdapter.read("/fdm/jsbsim/atmosphere/T-R", "OAT", "external temperature", "degC", Default.frecuency) { (it - 491.67) * (5.0/9.0) }

        // static pressure variable
        var staticPressure = csvAdapter.read("/fdm/jsbsim/atmosphere/P-psf", "Ps", "static pressure", "Pa", Default.frecuency) { it * 47.88026 }

        // heading variable
        var heading = csvAdapter.read("/fdm/jsbsim/attitude/psi-rad", "heading", "geographic heading", "radians", Default.frecuency)

        // Measured dynamic pressure variable
        var measuredPressure = Variable("measuredPressure","measured pressure", "Pa", dynamicPressure, sensorModel::measure)

        // indicated air speed
        val IAS = IASCalculator().calculate(measuredPressure)

        // true air speed variable
        val TAS = TASCalculator().calculate(IAS, OAT, staticPressure)

        // Position variables
        var latitudes = csvReader.readVariable("/fdm/jsbsim/position/lat-gc-rad")
        var longitudes = csvReader.readVariable("/fdm/jsbsim/position/long-gc-rad")
        var altitudes = csvReader.readVariable("/fdm/jsbsim/position/h-sl-meters")

        val csvWriter = CSVWriter(File("filteredVars.csv"))
        csvWriter.write(realTime, heading, dynamicPressure, IAS, TAS, measuredPressure, staticPressure, OAT)

        // discard first samples (first speed cycle)
        realTime = realTime.discardLast(discardSamples)
        heading = heading.discardFirst(discardSamples)
        dynamicPressure = dynamicPressure.discardFirst(discardSamples)
        measuredPressure = measuredPressure.discardFirst(discardSamples)
        staticPressure = staticPressure.discardFirst(discardSamples)
        OAT = OAT.discardFirst(discardSamples)
        latitudes = latitudes.drop(discardSamples).toDoubleArray()
        longitudes = longitudes.drop(discardSamples).toDoubleArray()
        altitudes = altitudes.drop(discardSamples).toDoubleArray()

        val referencePoint = Geodesic(latitudes[0], longitudes[0], altitudes[0])
        val xs = ArrayList<Double>()
        val ys = ArrayList<Double>()

        for(i in 0.until(latitudes.size)){
            val geodesicPosition = Geodesic(latitudes[i], longitudes[i], altitudes[i])
            val geocentricPosition = Transforms.geodesicToGeocentric(geodesicPosition)
            val localCartesian = Transforms.geocentricToCartesian(geocentricPosition, referencePoint)
            xs.add(localCartesian.x)
            ys.add(localCartesian.y)
        }
        val X = Variable("X","pos X", "m", Default.frecuency, xs.toDoubleArray())
        val Y = Variable("Y","pos Y", "m", Default.frecuency, ys.toDoubleArray())

        return Generator.VariableHolder(realTime, heading, dynamicPressure, measuredPressure, staticPressure, OAT, X, Y)
    }
}