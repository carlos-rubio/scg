/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.validation

import net.xnor.scg.calculators.BezierSensor
import net.xnor.scg.model.Sensor
import net.xnor.scg.synthetic.providers.ConstantWind
import net.xnor.scg.util.Config
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import java.io.Writer

/**
 * Set atmosphere/turbulence/milspec/severity = 1 or 2 in the JSBSim script template (script_C310_template.xml)
 */
fun main(){
    //val output = FileWriter(File(Config.outputFolder, "validationGridTurbulence_sev_1.csv"))
    val output = PrintWriter(System.out)
    with(ValidationTurbulence(output)){
        printHeader()
        execute(2.0, 2.5)
        //executeGrid()
    }
    output.close()
}

class ValidationTurbulence(output: Writer) : AbstractValidationCase(output) {

    override val gridSize: Int = 10

    override val simulatedSensor: Sensor = BezierSensor(k1 = 130.0, k2 = -145.0, k3 = -125.0, normalize = 3000.0)

    override val simulatedWind: ConstantWind = ConstantWind(15.0, 248.7549)

    override val sensorMinSpeed: Double = 10.0

    override val sensorMaxSpeed: Double = 50.0
}