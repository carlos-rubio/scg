/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.validation

import net.xnor.scg.calculators.BezierSensor
import net.xnor.scg.calculators.DynamicPressureCalculator
import net.xnor.scg.model.Sensor
import net.xnor.scg.model.Variable
import net.xnor.scg.model.WindModel
import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.pso.PsoSolver
import net.xnor.scg.pso.SolutionReader
import net.xnor.scg.report.ErrorUtil
import net.xnor.scg.synthetic.Generator
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.synthetic.providers.ConstantWind
import java.io.FileOutputStream
import java.io.Writer
import kotlin.math.sqrt

abstract class AbstractValidationCase(private val output: Writer) {

    // Grid size
    abstract val gridSize: Int

    // Simulated sensor
    abstract val simulatedSensor: Sensor

    // Simulated wind
    abstract val simulatedWind: ConstantWind

    abstract val sensorMinSpeed: Double
    abstract val sensorMaxSpeed: Double

    fun executeGrid( errorFn: (String)-> Unit = {}){
        for(i in 0.until(gridSize)) {
            for (j in 0.until(gridSize)) {
                executeTestPoint(i,j,errorFn)
            }
        }
    }

    private fun executeTestPoint(i: Int, j: Int, errorFn: (String)-> Unit = {}) {
        try {
            val step = 3.5 / (gridSize-1) // 0.5 to 4.0 cycles, span = 3.5
            val airspeedCycles = 0.5 + step * i
            val turns = 0.5 + step * j
            execute(airspeedCycles, turns)
        } catch (t:Throwable){
            errorFn(t.toString())
        }
    }

    fun printHeader() = output.write("cycles,turns,meanIas,maxIas,meanError,maxError, speedError, directionError, cost\n")

    fun execute(airspeedCycles: Double, turns: Double) {
        //println("airspeedCycles: $airspeedCycles, turns $turns")

        // Execute JSBSim flight
        val flightSimulator = JSBSimExecutor()
        val discardSamplesCount = flightSimulator.execute(airspeedCycles, turns)

        // Read JSBSim output and generate variables for PSO solver input
        val variableConverter = VariableConverter()
        val variables = variableConverter.convert(flightSimulator.dataFile, discardSamplesCount, simulatedSensor)

        // PSO configuration
        val psoConfig = PSOConfig.defaultConfig()
        val axes = Default.axes

        // Instantiate PSO solver
        val solver = PsoSolver() // OEM-CUDA PSO solver

        // Generate PSO input
        val generator = Generator() // Generator needed to create the solver input file
        generator.generate(variables, psoConfig, axes, solver.tempInputFile)

        // Execute PSO solver
        solver.execute()

        // Read the PSO solver result
        val solutionReader = SolutionReader()
        val solution = solutionReader.read(solver.outputFile, axes.size)

        val bestSolution = solution.globalMinimums.sortedBy { it.cost }.first()
        //println(bestSolution)
        //solution.globalMinimums.forEach { println(it) }

        val estimatedSensor = BezierSensor(bestSolution.position[2].toDouble(), bestSolution.position[3].toDouble(), bestSolution.position[4].toDouble(), 3000.0)

        val errorUtil = ErrorUtil(sensorMinSpeed, sensorMaxSpeed)
        val maxIasErrorSensor = errorUtil.maxAbsErrorSpeed(simulatedSensor, estimatedSensor)
        val meanIasErrorSensor = errorUtil.meanAbsErrorSpeed(simulatedSensor, estimatedSensor)

        val maxAbsError = errorUtil.maxAbsError(simulatedSensor, estimatedSensor)
        val meanAbsError = errorUtil.meanAbsError(simulatedSensor, estimatedSensor)

        val speedError = bestSolution.position[0].toDouble() - simulatedWind.speed
        val directionError = bestSolution.position[1].toDouble() - simulatedWind.dir

        output.write("$airspeedCycles, $turns, $meanIasErrorSensor, $maxIasErrorSensor, $meanAbsError, $maxAbsError, $speedError, $directionError, ${bestSolution.cost}\n")
        output.flush()

        // Estimated and measured trajectory
        /*
        val positionIntegrator = PositionIntegrator()
        val windProvider = ConstantWind(bestSolution.position[0].toDouble(), bestSolution.position[1].toDouble())
        val windModel = WindModel(windProvider.magnitude, windProvider.direction)
        val dynamicPressure = Variable("measuredPressure","measured pressure", "Pa", variables.measuredPressure, estimatedSensor::inverse)
        val IAS = IASCalculator().calculate(dynamicPressure)
        val TAS = TASCalculator().calculate(IAS, variables.OAT, variables.staticPressure)
        val (X, Y) = positionIntegrator.integrate(variables.realTime, TAS, variables.heading, windModel)
        writeTrayectory(X, Y, variables.X, variables.Y)
        */

        /*
        println("Simulated sensor: $simulatedSensor")
        println("Estimated sensor: $estimatedSensor")

        // Calculate true air speed from X,Y, wind
*/
        val windProvider = ConstantWind(bestSolution.position[0].toDouble(), bestSolution.position[1].toDouble())
        val windModel = WindModel(windProvider.magnitude, windProvider.direction)
        val TASfromTrayectory = calculateTAS(variables.X, variables.Y, windModel)
        var dynamicPressureFromTrayectory = DynamicPressureCalculator().calculate(variables.OAT, variables.staticPressure, TASfromTrayectory)
        val foutq = FileOutputStream("qtas.csv")
        val measuredWithEstimatedSensor = Variable("measuredPressure","q with estimated sensor","Pa", variables.dynamicPressure,estimatedSensor::measure)
        val measuredWithSimulatedSensor = Variable("measuredPressure","q with estimated sensor","Pa", variables.dynamicPressure,simulatedSensor::measure)

        for(k in 0.until(dynamicPressureFromTrayectory.size)){
            if(k % 20 == 0) {
                foutq.write("${dynamicPressureFromTrayectory[k]}, ${variables.dynamicPressure[k]}, ${measuredWithEstimatedSensor[k]},${measuredWithSimulatedSensor[k]}\n".toByteArray())
            }
        }
        foutq.close()

    }


private fun calculateTAS(x: Variable, y: Variable, windModel: WindModel) : Variable {
    val TAS = DoubleArray(x.size)
    for(i in 1.until(x.size)){
        val dx = x[i] - x[i-1]
        val dy = y[i] - y[i-1]
        val speedX = dx * Default.frecuency - windModel.x(i)
        val speedY = dy * Default.frecuency - windModel.y(i)
        TAS[i] = sqrt(speedX*speedX + speedY*speedY)
    }
    return Variable("TAS","TAS from trayectory", "m/s", Default.frecuency,TAS)
}

/*
fun writeTrayectory(Xi: Variable, Yi: Variable, Xs: Variable, Ys: Variable){
    val fout = FileOutputStream("trayectory.csv")
    for(i in 0.until(Xi.size)){
        val line = "${Xi[i]},${Yi[i]},${Xs[i]},${Ys[i]}\n"
        fout.write(line.toByteArray())
    }
    fout.close()
}*/

}