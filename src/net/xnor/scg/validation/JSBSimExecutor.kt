/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.validation

import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.util.Config
import net.xnor.scg.util.readToString
import java.io.File
import java.io.FileOutputStream
import kotlin.math.PI

/**
 * Execute JSBSim validation test
 * @param samplesPerTurn number of samples per heading turn. Default 4800 samples that correspond to a 4 minutes turn
 * @param speedWlimit maximum speed function angular speed. Default no limit (max value), usual value 0.07
 *
 * @author Carlos Rubio Sierra
 */
class JSBSimExecutor(private val samplesPerTurn: Int = 4800, private val speedWlimit: Double = Double.MAX_VALUE) {

    // Paths
    private val scriptTemplateFile = "${Config.jsbSimFolder}/scripts/script_C310_template.xml"
    private val scriptFile = "${Config.jsbSimFolder}/scripts/script_C310.xml"
    val dataFile = File("${Config.jsbSimFolder}/logged_data/validation_c310.csv")

    /** Return samples to discard */
    fun execute(airspeedCycles: Double, turns: Double) : Int {
        // step 1, generate script
        val discardSamplesCount = generateScript(airspeedCycles, turns)

        // step 2, execute JSBSim
        executeJSBSim()

        return discardSamplesCount
    }

    /**
     * Create JSBSim script from the template
     * @param airspeedCycles number of airspeed cycles of the maneuver
     * @param turns number of heading turns of the maneuver
     * @return the number of samples to discard (first speed cycle)
     */
    private fun generateScript(airspeedCycles: Double, turns: Double) : Int{

        var scriptTemplate = File(scriptTemplateFile).readToString()
        var requiredTime = (turns * samplesPerTurn)/ Default.frecuency
        val speedW = ((airspeedCycles) * 2.0 * PI) / requiredTime

        // increment required time with one complete speed cycle
        val speedCyclePeriod = (2.0 * PI) / speedW
        requiredTime += 1.0 * speedCyclePeriod

        if(speedW > speedWlimit)
            throw Exception("Speed W $speedW bigger than limit $speedWlimit")

        val headingRate = 360.0 / (samplesPerTurn / Default.frecuency)
        scriptTemplate = scriptTemplate.replace("##headingRate##", headingRate.toString())

        scriptTemplate = scriptTemplate.replace("##duration##", requiredTime.toString())
        scriptTemplate = scriptTemplate.replace("##speedW##", speedW.toString())

        val out = FileOutputStream(scriptFile)
        out.write(scriptTemplate.toByteArray())
        out.close()

        return (1.0 * speedCyclePeriod * Default.frecuency).toInt()
    }

    /**
     * Execute JSBSim process
     */
    private fun executeJSBSim(){
        val outputFile = dataFile
        outputFile.delete()
        val process = Runtime.getRuntime().exec("../JSBSim --script=$scriptFile", null, File(Config.jsbSimFolder))
        val result = process.waitFor()
        if(result != 0) throw Exception("Error, result code : $result")
    }
}