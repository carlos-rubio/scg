/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.pso

import net.xnor.scg.util.Config
import java.io.File

/**
 * Encapsulate OEM-CUDA PSO solver execution
 * */
class PsoSolver {

    // solver folder, is also the execution folder
    private val path = File(Config.oemCudaFolder)

    // solver input file path
    val tempInputFile = File(Config.oemCudaTempInputFile)

    // solver output file name
    private val output = "out.dat"

    // solver output file absolute path
    val outputFile
        get() = File(path, output)

    /**
     * Execute the solver and wait for complete execution.
     * In case of process error code, throws Exception.
     * The solver uses the tempInputFile as input and uses output file name to write the result.
     */
    fun execute(){
        outputFile.delete()
        val process = Runtime.getRuntime().exec("./oem-pso ${tempInputFile.absolutePath} -o $output", null, path)
        val result = process.waitFor()
        if(result != 0) throw Exception("Error, result code : $result")
    }
}