/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.pso

import net.xnor.scg.model.Variable
import net.xnor.scg.util.writeln
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

/**
 * Writes the input file needed by the solver
 */
class WindBoxWritter {

    fun write(psoConfig: PSOConfig, output: File, variables: List<Variable>, x0: Double, y0: Double,
                      casIntercept: Double, casK: Double, l1T: Double, normalize: Double, axes: List<Axis>){

        val params = FloatArray(6)
        params[0] = x0.toFloat()
        params[1] = y0.toFloat()
        params[2] = casIntercept.toFloat()
        params[3] = casK.toFloat()
        params[4] = l1T.toFloat()
        params[5] = normalize.toFloat()

        val fout = FileOutputStream(output)
        writeContent(fout, psoConfig, params, axes, variables)
        fout.close()
    }

    private fun writeContent(out: OutputStream, config: PSOConfig, values: FloatArray, axes: List<Axis>,variables: List<Variable>) {

        out.writeln("group: ${config.group}")
        out.writeln("kernel: ${config.kernel}")
        out.writeln("dimensions: ${config.dimensions}")
        out.writeln("swarms: ${config.swarms}")
        out.writeln("particles: ${config.particles}")
        out.writeln("c1: ${config.c1}")
        out.writeln("c2: ${config.c2}")
        out.writeln("c3: ${config.c3}")
        out.writeln("maxIter: ${config.iterations}")
        out.writeln("costLimit: ${config.costLimit}")
        out.writeln("sizeLimit: ${config.sizeLimit}")
        out.writeln("informantSize: ${config.informantSize}")
        out.writeln("numValues: ${values.size}")
        out.writeln("numVariables: ${variables.size}")
        out.writeln("sizeVariables: ${getSize(variables)}")

        for (value in values) {
            out.writeln("value: $value")
        }

        for ((min, max) in axes) {
            out.writeln("axisMin: $min")
            out.writeln("axisMax: $max")
        }

        for(i in 0 until getSize(variables)) {
            val line = variables.map { it.data[i] }.joinToString(separator = " ")
            out.writeln(line)
        }
    }

    private fun getSize(variables: List<Variable>): Int {
        return if (variables.isEmpty()) 0 else variables[0].size
    }
}