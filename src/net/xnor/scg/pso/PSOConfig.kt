/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.pso

/**
 * Particle swarm optimization configuration
 */
data class PSOConfig(
    val group: String,   // solver group
    val kernel: String,  // solver kernel
    val dimensions: Int, // dimensions in the model space
    val swarms: Int,     // number of launched swarms
    val particles: Int,  // number of particles
    val c1: Double,      // inertia term
    val c2: Double,      // local acceleration term
    val c3: Double,      // global acceleration term
    val iterations: Int, // maximum iterations
    val costLimit: Double, // limit to store cost
    val sizeLimit: Int,  // stored minima size limit (bytes)
    val informantSize: Int // informant size
) {
    companion object {
        fun defaultConfig(swarms: Int = 6, iterations: Int = 200) = PSOConfig(
            group = "basic-pso",
            kernel = "windBoxBezier",
            dimensions = 5,
            swarms = swarms,
            particles = 800,
            c1 = 0.7,
            c2 = 1.47,
            c3 = 1.47,
            iterations = iterations,
            costLimit = 0.1,
            sizeLimit = 1000, // number of hypercube points
            informantSize = 400
        )
    }

}