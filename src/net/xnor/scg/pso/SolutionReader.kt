/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.pso

import org.apache.commons.io.input.SwappedDataInputStream
import java.io.File
import java.io.FileInputStream

class SolutionReader {

    fun read(file: File, dimension: Int) : SolverSolution {
        val fin = FileInputStream(file)
        val din = SwappedDataInputStream(fin)

        val swarms = din.readInt()
        val solution = SolverSolution(swarms)
        for(i in 0 until swarms){
            val cost = din.readFloat()
            val coordinates = FloatArray(dimension){din.readFloat()}
            solution.add(SwarmGlobalMinimum(cost, Position(coordinates)))
        }

        din.close()
        fin.close()
        return solution
    }
}