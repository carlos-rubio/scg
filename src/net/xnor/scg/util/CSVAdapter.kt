/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.util

import net.xnor.scg.model.Variable

/**
 * Encapsulate CSVReader to include filtering
 * @author Carlos Rubio Sierra
 */
class CSVAdapter(private val csvReader: CSVReader) {

    fun read(csvName: String, name: String, description: String, units: String, frequency: Double) : Variable {
        val data = csvReader.readVariable(csvName)
        return Variable(name, description, units, frequency, data)
    }

    fun read(csvName: String, name: String, description: String, units: String, frequency: Double, mapFn: (Double) -> Double) : Variable {
        val data = csvReader.readVariable(csvName).map(mapFn).toDoubleArray()
        return Variable(name, description, units, frequency, data)
    }
}