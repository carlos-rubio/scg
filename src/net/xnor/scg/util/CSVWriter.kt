/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.util

import net.xnor.scg.model.Variable
import java.io.File
import java.io.FileOutputStream

/**
 * Writes a csv file
 * @author Carlos Rubio Sierra
 */
class CSVWriter(private val file: File) {

    fun write(vararg variables: Variable){
        val out = FileOutputStream(file)

        val header = variables.joinToString(separator = ",") { it.name }
        out.write(header+"\n")

        for(i in 0.until(variables[0].size)){
            val line = variables.map { it.data[i] }.joinToString(separator = ",")
            out.write(line+"\n")
        }

        out.close()
    }
}