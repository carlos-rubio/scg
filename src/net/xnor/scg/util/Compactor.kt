/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.util

import java.io.*
import kotlin.math.sqrt

/**
 * Compact a multidimensional sensitivity analysis, understanding 'compact' as replacing the multliple swarms
 * lines for the same case to one single line representing the case:
 *
 *  The coordinates of the line are the coordinates of the case
 *  The swarm number in the resulting line is set to 0
 *  The cost is the mean of the costs
 *  The real parameter is the mean of the swarm's lines real parameters
 *  The estimated parameter it the mean of the swarm's lines estimated parameters
 *  The error parameter is computed as typical deviation (sqrt of the: mean squared error/size)
 *  The mean absolute pressure error is computed as the mean absolute pressure error of the swarm's lines
 *  The maximum absolute pressure error is computed as the mean absolute pressure error of the swarm's lines
 *  The maximum absolute airspeed error is computed as the mean absolute airspeed error of the swarm's lines
 *
 * */
class Compactor {

     /**
      * Compact a multidimensional sensitivity analysis
      * */
     fun compact(inputFile: File, outputFile: File, dimensions: Int, parameters: Int) {

         val reader = BufferedReader(FileReader(inputFile))
         val writer = FileWriter(outputFile)

         val header = reader.readLine()
         writer.write(header + "\n")

         val block = Block(dimensions, parameters)

         var line = reader.readLine()
         while (line != null){
            val register = PrecisionLine(line, dimensions, parameters)

            if(register.swarm == 0) {
                if(!block.isEmpty) block.write(writer)
                block.init(register)
            } else {
                block.add(register)
            }

             line = reader.readLine()
         }

         if(!block.isEmpty) block.write(writer)

         reader.close()
         writer.close()
     }

    private class PrecisionLine(val line: String, val dimensions: Int, val parameters: Int){

        val tokens = line.split(",")

        fun coordinate(dimension: Int) = tokens[dimension].toDouble()

        val swarm = tokens[dimensions].toInt()

        val cost = tokens[dimensions+1].toDouble()

        fun real(parameter: Int) = tokens[dimensions+2+parameter*3].toDouble()

        fun estimated(parameter: Int) = tokens[dimensions+3+parameter*3].toDouble()

        fun error(parameter: Int) = tokens[dimensions+4+parameter*3].toDouble()

        val meanAbsErrorSensor = tokens[dimensions + parameters*3 + 2].toDouble()

        val maxErrorSensor = tokens[dimensions + parameters*3 + 3].toDouble()

        val maxIasErrorSensor = tokens[dimensions + parameters*3 + 4].toDouble()

    }

    private class Block(val dimensions: Int, val parameters: Int) {

        var size = 0

        val isEmpty get() = size == 0

        var cost = 0.0
        var real = DoubleArray(parameters)
        val estimated = DoubleArray(parameters)
        val squaredError = DoubleArray(parameters)
        var meanAbsErrorSensor = 0.0
        var maxErrorSensor = 0.0
        var maxIasErrorSensor = 0.0

        var lastLine : PrecisionLine? = null

        fun init(line: PrecisionLine){
            lastLine = line

            cost = 0.0
            for(i in 0 until parameters){
                real[i] = line.real(i)
                estimated[i] = line.estimated(i)
                squaredError[i] = line.error(i)*line.error(i)
            }
            meanAbsErrorSensor = line.meanAbsErrorSensor
            maxErrorSensor = line.maxErrorSensor
            maxIasErrorSensor = line.maxIasErrorSensor

            size = 1
        }

        fun add(line: PrecisionLine){
            lastLine = line

            cost += line.cost
            for(i in 0 until parameters){
                real[i] += line.real(i)
                estimated[i] += line.estimated(i)
                squaredError[i] += line.error(i)*line.error(i)
            }
            meanAbsErrorSensor += line.meanAbsErrorSensor
            maxErrorSensor += line.maxErrorSensor
            maxIasErrorSensor += line.maxIasErrorSensor

            ++size
        }

        fun write(out: Writer){
            lastLine?.let { register ->
                var line = ""

                for (i in 0 until dimensions) {
                    line += register.coordinate(i).toString() + ","
                }

                line += "0,$cost," // swarm is always 0

                for (i in 0 until parameters) {
                    val r = real[i]/size
                    val p = estimated[i]/size
                    val e = sqrt(squaredError[i]/size)

                    line += "$r,$p,$e,"
                }

                line += "${meanAbsErrorSensor/size},${maxErrorSensor/size},${maxIasErrorSensor/size}\n"

                out.write(line)
            }
        }
    }
}

fun main(){
    val files = arrayOf("B12","B13","B14","B23","B24","B34","B34Noise")

    files.forEach {
        Compactor().compact(File("${Config.outputFolder}/${it}.dat"), File("${Config.outputFolder}/${it}C.dat"), dimensions = 2, parameters = 5)
    }
}