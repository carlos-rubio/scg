/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.util

import java.io.File
import java.io.FileInputStream
import java.io.OutputStream

/**
 * Write text to output stream
 */
fun OutputStream.write(text: String){
    write(text.toByteArray())
}

/**
 * Write line to output stream
 */
fun OutputStream.writeln(text: String){
    write(text+"\n")
}

fun Int.pow(exponent: Int) =
    if(exponent == 0) 1
    else {
        var ret = this
        for (i in 1 until exponent) ret *= this
        ret
    }

/**
 * Reads content to a string
 */
fun File.readToString(): String {
    val fin = FileInputStream(this)
    val buffer = ByteArray(fin.available())
    fin.read(buffer)
    fin.close()
    return String(buffer)
}
