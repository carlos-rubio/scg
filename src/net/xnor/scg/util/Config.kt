/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.util

object Config {

    private val home = System.getProperty("user.home")

    /** Analysis output folder */
    val outputFolder = "$home/research/dataset/airspeedWind"

    /** OEM-CUDA PSO solver folder */
    val oemCudaFolder = "$home/research/OEM-CUDA-PSO"

    /** Temporary OEM CUDA input file */
    val oemCudaTempInputFile = "$oemCudaFolder/windBox.txt"

    /** JSBSim folder */
    val jsbSimFolder = "$home/research/jsbsim/jsbsim-code/workspace"
}