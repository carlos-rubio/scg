/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.util

import java.io.*
import java.util.ArrayList

/**
 * Reads a csv file
 * @author Carlos Rubio Sierra
 */
class CSVReader(file: File) {

    private val names = ArrayList<String>()
    private val contents = HashMap<String, MutableList<Double>>()

    init {
       val input = BufferedReader(FileReader(file))
       val header = input.readLine()
       names.addAll(header.split(","))

       names.forEach {name ->
           contents[name] = ArrayList()
       }

       var line = input.readLine()
       while (line != null){
            val values = line.split(",").map { it.toDouble() }
            values.forEachIndexed { index, value ->
                contents[names[index]]?.add(value)
            }
           line = input.readLine()
       }
    }

    fun readVariable(name: String): DoubleArray {
        return contents[name]?.toDoubleArray() ?: DoubleArray(0)
    }
}