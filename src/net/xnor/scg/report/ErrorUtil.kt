/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.report

import net.xnor.scg.model.Sensor
import net.xnor.scg.synthetic.cases.Default
import kotlin.math.abs
import kotlin.math.sign
import kotlin.math.sqrt

/**
 * Utility class
 */
class ErrorUtil(private val sensorMinSpeed : Double = Default.sensorMinSpeed, private val sensorMaxSpeed : Double = Default.sensorMaxSpeed) {

    /**
     * Gets the error and correct it for angle values
     */
    fun error(estimated: Double, real: Double, isAngle: Boolean) : Double {
        return if(isAngle){
            var diff = estimated -real
            if(diff > 180) diff -= 360 else if(diff < -180) diff += 360
            diff
        } else {
            estimated - real
        }
    }

    fun meanAbsError(realSensor: Sensor, estimatedSensor: Sensor) : Double{
        val qs = SensorIterator(sensorMinSpeed, sensorMaxSpeed).asSequence()
        return qs.map { q -> abs(realSensor.measure(q) - estimatedSensor.measure(q)) }.average()
    }

    fun maxAbsError(realSensor: Sensor, estimatedSensor: Sensor) : Double{
        val qs = SensorIterator(sensorMinSpeed, sensorMaxSpeed).asSequence()
        return qs.map { q -> abs(realSensor.measure(q) - estimatedSensor.measure(q)) }.max()!!
    }

    // return error in Knots
    fun meanAbsErrorSpeed(realSensor: Sensor, estimatedSensor: Sensor) : Double{
        val qs = SensorIterator(sensorMinSpeed, sensorMaxSpeed).asSequence()
        return qs.map { q -> abs(qToIAS(realSensor.measure(q)) - qToIAS(estimatedSensor.measure(q))) }.average()
    }

    // return error in Knots
    fun maxAbsErrorSpeed(realSensor: Sensor, estimatedSensor: Sensor) : Double{
        val qs = SensorIterator(sensorMinSpeed, sensorMaxSpeed).asSequence()
        return qs.map { q -> abs(qToIAS(realSensor.measure(q)) - qToIAS(estimatedSensor.measure(q))) }.max()!!
    }

    // Tranform from dynamic pressure to indicated airspeed in Knots
    private fun qToIAS(q: Double) = sign(q) * sqrt( (2.0* abs(q)) / 1.225) / 0.5144

    /**
     * Iterate  [min speed pressure,max speed pressure], including both extremes
     *
     */
    private class SensorIterator(val sensorMinSpeed : Double, val sensorMaxSpeed : Double, val repeat: Int = 100) : Iterator<Double> {

        /**
         * @param ias, indicated airspeed in m/s
         * @return dynamic pressure in Pa
         */
        private fun iasToQ(ias: Double) = 1.225 / 2.0 * ias * ias

        // pressure limits
        private val q1 = iasToQ(sensorMinSpeed)
        private val q2 = iasToQ(sensorMaxSpeed)
        private val range = q2 -q1

        private var index = 0

        override fun hasNext() = index < repeat

        override fun next() : Double {
            val q = q1 + range * (index.toDouble()/(repeat-1))
            ++index
            return q
        }
    }
}
