/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.report

/**
 * Store a list of error and provides util functions to calculate the mean, standard deviation and the mean of the
 * abs values of the errors
 * */
class ErrorList {

    private val values: MutableList<Double> = ArrayList()

    /**
     * Add an error to the list
     */
    fun add(error: Double) { values.add(error) }

    /**
     * Return the mean of the error values in the list
     */
    fun mean() = values.average()

    /**
     * Return the standard deviation of the errors of the list
     */
    fun sd(): Double {
        val mean = values.average()
        var variance = 0.0
        values.forEach { value ->
            variance += (value - mean) * (value - mean) / values.size
        }
        return Math.sqrt(variance)
    }

    /**
     * Returns the mean of the absolute value of the errors in the list
     */
    fun meanAbsError() : Double {
        var result = 0.0
        values.forEach { value ->
            result += Math.abs(value) / values.size
        }
        return result
    }
}