/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.report

import net.xnor.scg.calculators.BezierSensor
import net.xnor.scg.pso.SolverSolution
import net.xnor.scg.synthetic.cases.Case
import net.xnor.scg.synthetic.cases.Default

/**
 * Generates output report, as used in R
 */
class PrecisionExport {

    /**
     * Prints a summary of the export
     */
    fun print(case: Case, solution: SolverSolution){

        for(i in 0 until solution.swarms){
            println("Swarm: $i")
            val minimum = solution.globalMinimums[i]
            println("Cost: ${minimum.cost}")

            for(j in 0..case.parameters.dimension){
                val real = case.parameters[j]
                val estimated = minimum.position[j]
                print("Real: $real, estimated: $estimated")
            }
            println()
        }
    }

    /**
     * Gets the content of the output report, as used in R
     * */
    fun getLines(caseNumber: Int, case: Case, solution: SolverSolution) : String {
        return getLines(caseNumber.toString(), case, solution)
    }

    /**
     * Gets the content of the output report, as used in R
     * */
    fun getLines(coordinates: DoubleArray, case: Case, solution: SolverSolution) : String {
        val position = coordinates.joinToString(separator = ",")
        return getLines(position, case, solution)
    }

    /**
     * Gets the content of the output report, as used in R
     * */
    fun getLines(prefix: String, case: Case, solution: SolverSolution) : String {
        var ret = ""
        val errorUtil = ErrorUtil()

        for(swarm in 0 until solution.swarms){
            val minimum = solution.globalMinimums[swarm]

            ret += "$prefix,$swarm,${minimum.cost}"

            for( j in 0 until case.parameters.dimension){
                val isAngle = case.parameters.isAngle(j)
                val real = case.parameters[j]
                val estimated = minimum.position[j]
                val error = errorUtil.error(estimated.toDouble(), real, isAngle)

                ret += ",$real,$estimated,$error"
            }
            val estimatedSensor = BezierSensor(minimum.position[2].toDouble(),minimum.position[3].toDouble(),minimum.position[4].toDouble(), Default.normalize )
            val meanAbsErrorSensor = errorUtil.meanAbsError(case.sensorModel, estimatedSensor)
            val maxErrorSensor = errorUtil.maxAbsError(case.sensorModel, estimatedSensor)
            val maxIasErrorSensor = errorUtil.maxAbsErrorSpeed(case.sensorModel, estimatedSensor)
            ret += ",$meanAbsErrorSensor,$maxErrorSensor,$maxIasErrorSensor\n"
        }
        return ret
    }

    /**
     * Gets the output report table header
     */
    fun getHeader(dimensions: Int) : String{
        val prefix = if(dimensions == 1) "case"
        else IntArray(dimensions){ i -> i }.map { it -> "x"+it }.joinToString(separator = ",")
        return "$prefix,swarm,cost,R1,P1,E1,R2,P2,E2,R3,P3,E3,R4,P4,E4,R5,P5,E5,E6,E7,E8"
    }
}