/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.report

import net.xnor.scg.pso.Axis
import net.xnor.scg.pso.SolverSolution
import net.xnor.scg.synthetic.cases.Case

/**
 * Shows information about solver output
 */
class PrecisionSummary(private val axes: List<Axis>) {

    private val errors = Array(axes.size){ ErrorList() }

    /**
     * Print for each parameter, its normalized error mean, its normalized error standard deviation, and
     * its normalizer absolute error mean
     */
    fun print(){
        for((i, err) in errors.withIndex()){
            val range = axes[i].range
            val normalizedMean = 1000.0 * err.mean() / range
            val normalizedSd = 1000.0 * err.sd() / range
            val normalizedMeanAbsError = 1000.0 * err.meanAbsError() / range

            println("Parameter: ${(i+1)}, mean: $normalizedMean, Sd: $normalizedSd, absMean: $normalizedMeanAbsError")
        }
    }

    /**
     * Add the errors inside a solver solution
     */
    fun add(case: Case, solution: SolverSolution){
        for(swarm in 0 until solution.swarms){
            val minimum = solution.globalMinimums[swarm]
            val position = minimum.position
            for(dim in 0 until axes.size){
                val isAngle = case.parameters.isAngle(dim)
                val estimated = position[dim]
                val real = case.parameters[dim]
                val error = ErrorUtil().error(estimated.toDouble(), real, isAngle)
                errors[dim].add(error)
            }
        }
    }
}