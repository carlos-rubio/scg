/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.experiments

import net.xnor.scg.experiments.common.Analysis
import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.synthetic.WindBoxParameters
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.synthetic.cases.FixCase
import net.xnor.scg.synthetic.providers.ConstantTurn
import net.xnor.scg.synthetic.providers.SinusoidalSpeed

/**
 * Bi-dimensional analysis,
 * Speed function, meanSpeed and deviationSpeed
 */
class B14 : Analysis() {

    // Grid data
    override fun getIterator() = Analysis.MultiDimensionalIterator(dimensions = 2, repetitions = 21)

    override fun getCase(iterator: Iterator) : Case {
        println(iterator.coordinates().joinToString())
        return Case(iterator.coordinates())
    }

    override fun getConfig(iterator: Iterator) = PSOConfig.defaultConfig(swarms = 6)

    class Case(val coordinates: DoubleArray): FixCase(WindBoxParameters.default){

        override val speedProvider: SinusoidalSpeed
            get() {
                val meanSpeed = 20.0 + 20.0 * coordinates[0] // from 20.0 m/s to 40.0 m/s
                val deviationSpeed = Default.speedDeviation // from 0 to 20 m/s
                val speedCycles = Default.speedCycles

                // important: Set deviation to a fix value to not influence the precision
                return SinusoidalSpeed(meanSpeed, deviationSpeed, (speedCycles * 2.0 * Math.PI) / samples)
            }

        override val headingProvider: ConstantTurn
            get() = ConstantTurn(0.0, (turns * 2.0 * Math.PI) / samples)

        private val turns: Double get() = 0.5 + (3.5 * coordinates[1]) // from 0.5 to 4.0 turns

        private val samples: Int get() = (turns * 1333.33).toInt()
    }
}

fun main() {
    B14().execute()
}