/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.experiments.common

import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.pso.PsoSolver
import net.xnor.scg.pso.SolutionReader
import net.xnor.scg.report.PrecisionExport
import net.xnor.scg.report.PrecisionSummary
import net.xnor.scg.synthetic.Generator
import net.xnor.scg.synthetic.cases.Case
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.util.Config
import net.xnor.scg.util.pow
import net.xnor.scg.util.write
import net.xnor.scg.util.writeln
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.random.Random

abstract class Analysis {

    /**
     * Override to provide the case to the analysis iterator
     */
    protected abstract fun getIterator(): Iterator

    /**
     * Override to provide the case to the analysis engine
     */
    protected abstract fun getCase(iterator: Iterator): Case

    /**
     * Override to provide the PSO configuration to the analysis engine
     */
    protected abstract fun getConfig(iterator: Iterator): PSOConfig

    /**
     * The name of the case, it is used as the name of the results output file
     */
    protected val name: String get() = javaClass.simpleName

    /**
     * Execute the analysis
     * @param outFolder, the folder where the output file is written. This file is usually processed in R and its name
     * is based on the overridden name variable
     */
    fun execute(outFolder: File = File(Config.outputFolder)){
        val start = Date() // annotate start time

        val export = PrecisionExport() // used to write the output data file
        val summary = PrecisionSummary(Default.axes) // used to print result in console
        val solver = PsoSolver() // OEM-CUDA PSO solver
        val generator = Generator() // Generator needed to create the solver input file
        val solutionReader = SolutionReader() // solution reader

        val fout = FileOutputStream(File(outFolder, "$name.csv")) // output data file

        val iterator = getIterator()

        fout.writeln( export.getHeader(iterator.dimensions) ) // write table header

        while(iterator.available()){

            // obtain the case
            val case = getCase(iterator)

            // generate solver input file
            generator.generate(case, getConfig(iterator), solver.tempInputFile)

            // execute OEM-PSO solver
            solver.execute()

            // read the result
            val solution = solutionReader.read(solver.outputFile, case.dimensions)

            // add the solution to the summary
            summary.add(case, solution)

            // write the output data
            if(iterator.dimensions == 1)
                fout.write(export.getLines(iterator.index(), case, solution))
            else
                fout.write(export.getLines(iterator.coordinates(), case, solution))

            // advance iterator
            iterator.next()
        }
        fout.close()

        // print summary
        summary.print()

        // print ellapsed time
        val end = Date()
        val seconds = (end.time - start.time).toDouble()/1000.0
        println("Ellapsed time: $seconds s.")
    }

    /**
     * Analysis iterator
     * */
    interface Iterator {

        /**
         * Return the dimensions of the iterator
         */
        val dimensions: Int

        /**
         * Indicates if it is ready an iteration
         */
        fun available(): Boolean;

        /**
         * Advance to the next iteration
         */
        fun next()

        /**
         * Lineal iteration index, starting with 0
         */
        fun index(): Int

        /**
         * Dimensional coordinates, starting from 0 for the first iteration
         */
        fun coordinates(): DoubleArray
    }

    /**
     * One-dimensional iterator for n repetitions
     */
    class OneDimensionalIterator(val repetitions: Int, override  val dimensions: Int = 1, val dimension: Int = 0) : Iterator {

        var index = 0

        override fun available() = repetitions > index

        override fun next() { ++index }

        override fun index() = index

        override fun coordinates() : DoubleArray {
            val ret = DoubleArray(dimensions)
            ret[dimension] = index.toDouble() / (repetitions-1)
            return ret
        }
    }

    /**
     * Multi-dimensional iterator d dimensions x n repetitions
     */
    class MultiDimensionalIterator(override val dimensions: Int, val repetitions: Int) : Iterator {

        var index = 0

        val total = repetitions.pow(dimensions)

        override fun available() = total > index

        override fun next() { ++index }

        override fun index() = index

        override fun coordinates() : DoubleArray {
            val ret = DoubleArray(dimensions)

            for(i in 0 until dimensions){
                ret[i] = (( index / repetitions.pow(i) ) % repetitions).toDouble() / (repetitions-1)
            }

            return  ret
        }
    }

    /**
     * Random-dimensional iterator d dimensions x n repetitions
     */
    class RandomDimensionalIterator(override val dimensions: Int, val repetitions: Int) : Iterator {

        private var coord = randomCoordinates()

        var index = 0

        override fun available() = repetitions > index

        override fun next() {
            coord = randomCoordinates()
            ++index
        }

        override fun index() = index

        override fun coordinates() = coord

        private fun randomCoordinates() = DoubleArray(dimensions) { Random.nextDouble() }
    }
}