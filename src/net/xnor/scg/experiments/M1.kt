/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.experiments

import net.xnor.scg.experiments.common.Analysis
import net.xnor.scg.model.Sensor
import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.synthetic.WindBoxParameters
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.synthetic.cases.FixCase
import net.xnor.scg.synthetic.providers.*

/**
 * Multi dimensional sensitivity analysis
 *
 * Dimensions:
 *  Wind speed lineal change      (0 to 1 m/s)
 *  Wind direction lineal change  (0 to 5 degrees)
 *  Sensor noise sd               (0 to 50 Pa)
 *  Wind speed noise              (0 to 4 m/s)
 *  Wind direction noise          (0 to 10 degrees)
 */
class M1 : Analysis() {

    // Grid data
    //override fun getIterator() = Analysis.MultiDimensionalIterator(dimensions = 5, repetitions = 5)

    // Random test data
    override fun getIterator() = Analysis.RandomDimensionalIterator(dimensions = 5, repetitions = 500)

    //override fun getIterator() = Analysis.OneDimensionalIterator(repetitions = 11, dimensions = 5, dimension = 4)

    override fun getCase(iterator: Iterator) : Case {
        println(iterator.coordinates().joinToString())
        return Case(iterator.coordinates())
    }

    override fun getConfig(iterator: Iterator) = PSOConfig.defaultConfig(swarms = 6)

    class Case(val coordinates: DoubleArray): FixCase(WindBoxParameters.default){

        override val sensorModel: Sensor
            get() = SensorNoiseAdapter(50.0 * coordinates[2], super.sensorModel) // Sensor noise sd: (0 to 50 Pa)

        override val windProvider: WindProvider
            get() {
                val linearWind = LinearWind(
                    startSpeed = Default.windSpeed,
                    endSpeed = Default.windSpeed + coordinates[0] * 1.0, // (0 to 1 m/s)
                    startDirection = Default.windDirection,
                    endDirection = Default.windDirection + coordinates[1] * 5.0, // (0 to 5 degrees)
                    samples = discretizacion.samples)

                val noiseWind = WindNoiseAdapter(
                    sigmaSpeed = coordinates[3] * 4.0, // (0 to 4 m/s)
                    sigmaDirection = coordinates[4] * 10.0, // (0 to 10 degrees)
                    windProvider = linearWind)

                return noiseWind
            }
    }
}

fun main() {
    M1().execute()
}