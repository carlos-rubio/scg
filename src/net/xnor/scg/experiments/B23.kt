/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.experiments


import net.xnor.scg.experiments.common.Analysis
import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.synthetic.WindBoxParameters
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.synthetic.cases.FixCase
import net.xnor.scg.synthetic.providers.SinusoidalSpeed

/**
 * Bi-dimensional analysis,
 * Speed function, meanSpeed and deviationSpeed
 */
class B23 : Analysis() {

    // Grid data
    override fun getIterator() = Analysis.MultiDimensionalIterator(dimensions = 2, repetitions = 21)

    override fun getCase(iterator: Iterator) : Case {
        println(iterator.coordinates().joinToString())
        return Case(iterator.coordinates())
    }

    override fun getConfig(iterator: Iterator) = PSOConfig.defaultConfig(swarms = 6)

    class Case(val coordinates: DoubleArray): FixCase(WindBoxParameters.default){

        override val speedProvider: SinusoidalSpeed
            get() {
                val meanSpeed = Default.speedBase
                val deviationSpeed = 2.0 + 13.0 * coordinates[0] // from 2 to 15 m/s
                val speedCycles = 0.5 + (3.5 * coordinates[1]) // from 0.5 to 4 cycles

                // important: Set deviation to a fix value to not influence the precision
                return SinusoidalSpeed(meanSpeed, deviationSpeed, (speedCycles * 2.0 * Math.PI) / discretizacion.samples)
            }
    }
}

fun main() {
    B23().execute()
}