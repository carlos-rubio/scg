/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.experiments

import net.xnor.scg.experiments.common.Analysis
import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.synthetic.WindBoxParameters
import net.xnor.scg.synthetic.cases.Default
import net.xnor.scg.synthetic.cases.FixCase
import net.xnor.scg.synthetic.providers.SinusoidalSpeed

/**
 * Sensitivity analysis, aircraft speed cycles
 */
class S3 : Analysis() {

    override fun getIterator() = Analysis.OneDimensionalIterator(100)

    override fun getCase(iterator: Iterator) = Case(iterator.coordinates()[0])

    override fun getConfig(iterator: Iterator) = PSOConfig.defaultConfig()

    class Case(val progress: Double): FixCase(WindBoxParameters.default){

        override val speedProvider: SinusoidalSpeed
            get() {
                val speedCycles = 0.5 + (3.5 * progress) // from 0.5 to 4 cycles
                return SinusoidalSpeed(Default.speedBase, Default.speedDeviation, (speedCycles * 2.0 * Math.PI) / discretizacion.samples)
            }
    }
}

fun main() {
    S3().execute()
}