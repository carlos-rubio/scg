/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.calculators

import net.xnor.scg.model.Variable
import java.lang.IllegalArgumentException

/**
 * Base class for calculators.
 *
 * A calculator gets some variables and calculate another result variable
 *
 */
abstract class Calculator {

    /**
     * Calculate method
     * Checks if each variable has the units expected, in other case throw IllegalArgumentException
     *
     * @param variables One or more variables used to calculate the result variable
     *
     * @return The result variable
     *
     */
    fun calculate(vararg variables: Variable): Variable {

        for((index,variable) in variables.withIndex()){
            val expectedUnit = expectedUnits[index]
            if(!variable.unit.equals(expectedUnit, ignoreCase = true))
                throw IllegalArgumentException("${variable.name} need to be in $expectedUnit")
        }

        val freq = variables[0].frequency
        val size = variables[0].size
        val data = DoubleArray(size)

        for(i in 0 until size){
            val params = DoubleArray(variables.size){ j -> variables[j].data[i]}
            data[i] = calculate(*params)
        }

        return Variable(name, description, unit, freq, data)
    }

    protected abstract val expectedUnits : List<String>

    /**
     * Result variable name
     */
    protected abstract val name: String

    /**
     * Result variable description
     */
    protected abstract val description: String

    /**
     * Result variable unit
     */
    protected abstract val unit: String

    /**
     * Calculate one value of the result variable
     * @param params, one sample value for each of the input variables
     * @return one sample of the result variable
     */
    protected abstract fun calculate(vararg params: Double): Double

}