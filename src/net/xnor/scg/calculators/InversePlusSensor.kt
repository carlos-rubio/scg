/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.calculators

import net.xnor.scg.model.Sensor
import kotlin.math.sqrt

/**
 * Sensor model with inverse plus function
 */
class InversePlusSensor(private val k1: Double, private val k2: Double) : Sensor {

    /**
     * Measurement function
     *
     * @param real, the real value of the variable
     * @return the measured value by the sensor
     */
    override fun measure(real: Double): Double {
        return real + k1 + k2 / real
    }

    /**
     * Inverse function
     *
     * @param measurement, the measured value by the sensor
     * @return the real value of the variable
     */
    override fun inverse(measurement: Double) : Double {
        val fa = k1 - measurement
        val fb = sqrt(fa * fa - 4.0 * k2)
        return (-fa + fb) / 2.0
    }
}