/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.calculators

import net.xnor.scg.model.Sensor
import kotlin.math.sqrt

/**
 * Sensor model with Bezier quadratic curve
 */
class BezierSensor(private val k1: Double, private val k2: Double, private val k3: Double, private val normalize: Double) : Sensor {

    /**
     * Measurement function
     *
     * @param real, the real value of the variable
     * @return the measured value by the sensor
     */
    override fun measure(real: Double): Double {
        val t = real / normalize
        val error = k1*(1.0-t)*(1.0-t) + k2*2.0*t*(1.0-t) + k3*t*t
        return real + error
    }

    /**
     * Inverse function
     *
     * @param measurement, the measured value by the sensor
     * @return the real value of the variable
     */
    override fun inverse(measurement: Double) : Double {
        val sqnorm = normalize * normalize
        val a = (k1 / sqnorm) - (2.0 * k2 / sqnorm) + k3 / sqnorm
        val b = 1.0 - (2.0 * k1 / normalize) + (2.0 * k2 / normalize)
        val c = k1 - measurement
        return (-b + sqrt(b*b - 4*a*c)) / (2.0*a)
    }

    override fun toString(): String {
        return "BezierSensor(k1=$k1, k2=$k2, k3=$k3, normalize=$normalize)"
    }


}
