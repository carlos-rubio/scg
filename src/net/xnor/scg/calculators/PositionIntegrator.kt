/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.calculators

import net.xnor.scg.model.Variable
import net.xnor.scg.model.WindModel
import kotlin.math.cos
import kotlin.math.sin

/**
 * Integrates the trajectory from position {0,0}, given the true airspeed, instant heading and wind model.
 * Performs the integration with variable time step size using the realTime variable.
 */
class PositionIntegrator {

    /**
     * Integrates the trajectory
     * @param realTime, the sample times, s
     * @param TAS, the true air speed, m/s
     * @param heading, the instant heading, radians
     * @param windModel, the wind model
     * @return the variables X and Y with the list of the successive coordinates.
     */
    fun integrate(realTime: Variable, TAS: Variable, heading: Variable, windModel: WindModel, x0: Double = 0.0, y0: Double = 0.0) : Pair<Variable, Variable> {
        // Integrate position X,Y
        val samples = realTime.size
        val freq = realTime.frequency

        val xs = DoubleArray(samples)
        val ys = DoubleArray(samples)

        xs[0] = x0
        ys[0] = y0

        for(i in 1 until samples){
            val dt = realTime[i] - realTime[i-1]
            val angle = heading[i-1]
            val deltaX = (sin(angle) * TAS[i - 1] + windModel.x(i)) * dt
            val deltaY = (cos(angle) * TAS[i - 1] + windModel.y(i)) * dt
            xs[i] = xs[i-1] + deltaX
            ys[i] = ys[i-1] + deltaY
        }

        // Coordinate variables
        val x = Variable("X", "x position", "m", freq, xs)
        val y = Variable("Y", "y position", "m", freq, ys)

        return Pair(x, y)
    }

    /**
     * Return the length of the trajectory in norm 1
     **/
    fun norm1(X: Variable, Y: Variable): Double{
        var total = 0.0
        for (i in 1 until X.size) {
            val deltaX = X[i] - X[i - 1]
            val deltaY = Y[i] - Y[i - 1]
            total += Math.abs(deltaX) + Math.abs(deltaY)
        }
        return total
    }

}