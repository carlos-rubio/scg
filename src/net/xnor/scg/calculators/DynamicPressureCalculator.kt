/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.calculators

/**
 * Calculates the dynamic pressure given the air temperature, the static pressure and the
 * true airspeed.
 */
class DynamicPressureCalculator : Calculator() {

    override val expectedUnits: List<String>
        get() = listOf("degC", "Pa", "m/s")

    override val name: String get() = "q"

    override val description: String get() = "Dynamic pressure"

    override val unit: String get() = "Pa"

    override fun calculate(vararg params: Double): Double {
        val etemp = params[0] // external temperature in celsius
        val staticPressure = params[1] // Pa
        val tas = params[2] // tas in m/s

        val delta = 288.15 / (etemp + 273.15) * (staticPressure / 101325.0)
        val ias = tas * Math.sqrt(delta)
        return 1.225 / 2.0 * ias * ias
    }
}