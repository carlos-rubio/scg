/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.calculators

/**
 * Calculates the true airspeed, given the indicated air speed, the air temperature and the static pressure
 */
class TASCalculator: Calculator() {

    override val expectedUnits: List<String>
        get() = listOf("m/s", "degC", "Pa")

    override val name: String get() = "TAS"

    override val description: String get() = "True Airspeed"

    override val unit: String get() = "m/s"

    override fun calculate(vararg params: Double): Double {
        val ias = params[0] // ias in m/s
        val etemp = params[1] // external temperature in celsius
        val staticPressure = params[2]

        val delta = 288.15 / (etemp + 273.15) * (staticPressure / 101325.0)

        return ias / Math.sqrt(delta)
    }
}