/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.model

import kotlin.math.cos
import kotlin.math.sin

/**
 * Represents constant wind with some speed and direction
 * @param speed in m/s
 * @param orientation in degrees, 0 degrees = wind blows from north to south
 **/
class WindModel(private val speed: SampledFunction, private val orientation: SampledFunction) {

    /**
     * Returns the x speed component of the wind
     * coordinate system:
     *   y positive -> geographic north
     *   x positive -> geographic east
     **/
    fun x(i: Int): Double = - speed[i] * sin(Math.toRadians(orientation[i]))

    /**
     * Returns the y speed component of the wind
     * coordinate system:
     *   y positive -> geographic north
     *   x positive -> geographic east
     **/
    fun y(i: Int): Double = - speed[i] * cos(Math.toRadians(orientation[i]))
}