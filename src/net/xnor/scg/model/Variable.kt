/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.model

/**
 * Represents a discrete physical variable
 *
 * @param name, short name of the variable
 * @param description, its description
 * @param unit, its units
 * @param frequency, number of samples per second
 * @param data, a double array with the value of each sample
 */
class Variable(
    val name: String,
    val description: String,
    val unit: String,
    val frequency: Double,
    val data: DoubleArray
) {
    /**
     * This constructor creates a constant variable intialized with value and with size number of samples
     */
    constructor(name: String, description: String, unit: String, frequency: Double, value: Double, size: Int):
            this(name, description, unit, frequency, DoubleArray(size){ value })

    /**
     * This constructor transform the variable using a conversion function into another variable with the same name,
     * description, unit and frequency. Can be used to scale of offset the original variable
     */
    constructor(variable: Variable, convert: (Double) -> Double):
            this(variable.name, variable.description, variable.unit, variable.frequency, DoubleArray(variable.size){ i -> convert(variable.data[i])})

    /**
     * This constructor transform the variable using a conversion function into another variable with the same frequency, but a new name,
     * description and units.
     */
    constructor(name: String, description: String, unit: String, variable: Variable, convert: (Double) -> Double):
            this(name, description, unit, variable.frequency, DoubleArray(variable.size){ i -> convert(variable.data[i])})

    /**
     * The number of samples in the variable
     */
    val size: Int
        get() = data.size

    /**
     * Gets the i sample value
     */
    operator fun get(i: Int): Double = data[i]

    /**
     * Clone the variable discarding the first n samples
     */
    fun discardFirst(samples: Int) : Variable {
       return Variable(name, description, unit, frequency, data.drop(samples).toDoubleArray())
    }

    /**
     * Clone the variable discarding the last n samples
     */
    fun discardLast(samples: Int) : Variable {
        return Variable(name, description, unit, frequency, data.dropLast(samples).toDoubleArray())
    }

}