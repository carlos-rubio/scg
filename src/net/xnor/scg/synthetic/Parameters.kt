/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic

/**
 * Model space parameters abstraction
 */
interface Parameters {

    /** Number of dimensions of the model space */
    val dimension: Int

    /** Gets the parameter for one dimension */
    operator fun get(dimension: Int) : Double

    /** Indicate if the parameter of this dimension is an angle value */
    fun isAngle(dimension: Int) : Boolean
}