/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic

import net.xnor.scg.calculators.DynamicPressureCalculator
import net.xnor.scg.calculators.PositionIntegrator
import net.xnor.scg.calculators.TASCalculator
import net.xnor.scg.model.Variable
import net.xnor.scg.model.WindModel
import net.xnor.scg.pso.Axis
import net.xnor.scg.pso.PSOConfig
import net.xnor.scg.pso.WindBoxWritter
import net.xnor.scg.synthetic.cases.Case
import net.xnor.scg.synthetic.cases.Default
import java.io.File

/**
 * Creates the PSO solver input file for an experiments given the PSO configuration and the case
 */
@Suppress("LocalVariableName")
class Generator {

    private val positionIntegrator = PositionIntegrator()

    /**
     * Create the solver input
     * @param case, the case to re-create
     * @param psoConfig, the PSO solver configuration
     * @param output, the out file (input file for the solver)
     */
    fun generate(case: Case, psoConfig: PSOConfig, output: File){
        val variables = generateVariables(case)
        generate(variables, psoConfig, case.axes, output)
    }

    /**
     * Create the solver input
     * @param variables, data variables
     * @param psoConfig, the PSO solver configuration
     * @param axes, the parameter space axes definition
     * @param output, the out file (input file for the solver)
     */
    fun generate(variables: VariableHolder, psoConfig: PSOConfig, axes: List<Axis>, output: File){
        writeOemCudaSwarmFile(psoConfig,variables, axes, output)
    }

    /**
     * Create the variables for the experiments
     * @param case, the case to re-create
     */
    private fun generateVariables(case: Case): VariableHolder {

        val samples = case.discretizacion.samples
        val freq = case.discretizacion.frequency

        // time variable
        val times = DoubleArray(samples){ i ->  i * (1.0/freq) }
        val realTime = Variable("realTime", "time", "s", freq, times)

        // temperature variable
        val OAT = Variable("OAT", "external temperature", "degC", freq, case.environment.temperature, samples)

        // static pressure variable
        val staticPressure = Variable("Ps", "static pressure", "Pa", freq, case.environment.staticPressure, samples)

        // indicated air speed
        val IAS = Variable("IAS", "indicated airspeed", "m/s", freq, DoubleArray(samples){i -> case.speedProvider[i]})

        // true air speed variable
        val TAS = TASCalculator().calculate(IAS, OAT, staticPressure)

        // heading variable
        val heading = Variable("heading", "geographic heading", "radians", freq, DoubleArray(samples){i -> case.headingProvider[i]})

        // Dynamic pressure variable
        val dynamicPressure = DynamicPressureCalculator().calculate(OAT, staticPressure, TAS)

        // Measured dynamic pressure variable
        val measuredPressure = Variable("measuredPressure","measured pressure", "Pa", dynamicPressure, case.sensorModel::measure)

        // Position variables
        val windProvider = case.windProvider
        val windModel = WindModel(windProvider.magnitude, windProvider.direction)
        val (X, Y) = positionIntegrator.integrate(realTime, TAS, heading, windModel)

        return VariableHolder(realTime, heading, dynamicPressure, measuredPressure, staticPressure, OAT, X, Y)
    }

    /**
     * Writes the output file (input file for the solver)
     */
    private fun writeOemCudaSwarmFile(psoConfig: PSOConfig, variables: VariableHolder, axes: List<Axis>, output: File){

        val l1T = positionIntegrator.norm1(variables.X, variables.Y) // in norm 1
        val casIntercept = 0.0
        val casK = 1.0
        val normalize = Default.normalize

        val varList = with(variables) {
             listOf(realTime, heading, measuredPressure, staticPressure, OAT, X, Y)
        }

        WindBoxWritter().write(psoConfig, output, varList, variables.X[0], variables.Y[0], casIntercept, casK, l1T, normalize, axes)
    }

    /**
     * Utility holder class for the list of needed variables
     */
    data class VariableHolder(
        val realTime: Variable,
        val heading: Variable,
        val dynamicPressure: Variable,
        val measuredPressure: Variable,
        val staticPressure: Variable,
        val OAT: Variable,
        val X: Variable,
        val Y: Variable
    )
}