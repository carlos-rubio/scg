/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic.providers

import net.xnor.scg.model.SampledFunction

/**
 * Linear wind provider
 *
 * @param startSpeed, m/s
 * @param endSpeed, m/s
 * @param startDirection, degrees
 * @param endDirection, degrees
 * @param samples, number of samples
 *
 **/
class LinearWind(private val startSpeed: Double, private val endSpeed: Double, private val startDirection: Double, private val endDirection: Double, private val samples: Int) : WindProvider {

    val speedDrift = (endSpeed - startSpeed)/samples
    val directionDrift = (endDirection - startDirection)/samples

    override val magnitude = object : SampledFunction {
        override fun get(sample: Int)= startSpeed + (sample * speedDrift)
    }

    override val direction = object : SampledFunction {
        override fun get(sample: Int) = startDirection + (sample * directionDrift)
    }

    override val meanDirection : Double get() = (endDirection - startDirection)/2.0 + startDirection

    override val meanSpeed : Double get() =  (endSpeed - startSpeed)/2.0 + startSpeed

}