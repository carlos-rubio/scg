/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic.providers

import net.xnor.scg.model.SampledFunction
import java.util.*

/**
 * Wind noise, add noise to a wind provider
 *
 * @param sigmaSpeed, typical deviation for speed (Gaussian distribution), m/s
 * @param sigmaDirection, typical deviation for direction (Gaussian distribution), degrees
 **/
class WindNoiseAdapter(private val sigmaSpeed: Double, private val sigmaDirection: Double, private val windProvider: WindProvider) : WindProvider {

    private val random = Random()

    override val magnitude = object : SampledFunction {
        override fun get(sample: Int) = windProvider.magnitude[sample] + random.nextGaussian() * sigmaSpeed
    }

    override val direction = object : SampledFunction {
        override fun get(sample: Int)= windProvider.direction[sample] + random.nextGaussian() * sigmaDirection
    }

    override val meanDirection: Double get() = windProvider.meanDirection

    override val meanSpeed: Double = windProvider.meanSpeed
}