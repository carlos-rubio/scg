/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic.providers

import net.xnor.scg.model.SampledFunction

/**
 * Constant wind provider
 * @param speed, wind speed, m/s
 * @param dir, wind direction, degrees
 * */
class ConstantWind(val speed: Double, val dir: Double): WindProvider {

    override val magnitude = object: SampledFunction {
        override fun get(sample: Int) = speed
    }

    override val direction = object: SampledFunction {
        override fun get(sample: Int) = dir
    }

    override val meanDirection: Double get() = dir

    override val meanSpeed: Double get() = speed
}