/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic

import net.xnor.scg.synthetic.cases.Default

/**
 * Wind box with inverse plus sensor model parameters
 */
class WindBoxParameters(
    val windSpeed: Double,     // m/s
    val windDirection: Double, // degrees
    val sensorK1: Double,      // Pa
    val sensorK2: Double,      //
    val sensorK3: Double       // Pa
) : Parameters {

    /** Number of dimensions of the model space */
    override val dimension = 5

    /** Gets the parameter for one dimension */
    override fun get(dimension: Int) = when(dimension) {
        0 -> windSpeed
        1 -> windDirection
        2 -> sensorK1
        3 -> sensorK2
        4 -> sensorK3
        else -> Double.NaN
    }

    /** Indicate if the parameter of this dimension is an angle value */
    override fun isAngle(dimension: Int) = (dimension == 1)

    companion object {
        val default = WindBoxParameters(Default.windSpeed, Default.windDirection, Default.sensorK1, Default.sensorK2, Default.sensorK3)
    }
}