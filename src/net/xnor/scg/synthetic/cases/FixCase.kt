/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic.cases

import net.xnor.scg.calculators.BezierSensor
import net.xnor.scg.model.Sensor
import net.xnor.scg.synthetic.Parameters
import net.xnor.scg.synthetic.WindBoxParameters
import net.xnor.scg.synthetic.providers.*

open class FixCase(private val params: WindBoxParameters) : Case {

    override val dimensions = 5

    override val axes = Default.axes

    override val discretizacion get() = Discretizacion(Default.samples, Default.frecuency)

    override val environment get() = Default.environment

    override val sensorModel : Sensor get() = BezierSensor(params.sensorK1, params.sensorK2, params.sensorK3, Default.normalize)

    override val speedProvider : SpeedProvider get() = SinusoidalSpeed(Default.speedBase, Default.speedDeviation, (Default.speedCycles *2.0 * Math.PI) / discretizacion.samples)

    override val headingProvider: HeadingProvider get() = ConstantTurn(0.0, (Default.turns *2.0 * Math.PI) / discretizacion.samples)

    override val windProvider: WindProvider get() = ConstantWind(params.windSpeed, params.windDirection)

    override val parameters : Parameters get() = params
}