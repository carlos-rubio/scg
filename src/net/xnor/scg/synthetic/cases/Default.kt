/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic.cases

import net.xnor.scg.pso.Axis
import net.xnor.scg.synthetic.Environment

object Default {

    // Axes limits
    private const val minWindSpeed = 0.0f  // wind speed m/s
    private const val maxWindSpeed = 30.0f // wind speed m/s
    private const val minWindDirection = 0.0f   // wind direction, degrees
    private const val maxWindDirection = 360.0f // wind direction, degrees
    private const val minSensorK1 = -500.0f  // sensor K1
    private const val maxSensorK1 = 500.0f   // sensor K1
    private const val minSensorK2 = -500.0f  // sensor K2
    private const val maxSensorK2 = 500.0f   // sensor K2
    private const val minSensorK3 = -500.0f  // sensor K3
    private const val maxSensorK3 = 500.0f   // sensor K3


    // Axes
    val axes = listOf(
        Axis(minWindSpeed,     maxWindSpeed),     // wind speed, m/s
        Axis(minWindDirection, maxWindDirection), // wind direction, degrees
        Axis(minSensorK1,      maxSensorK1),      // sensor K1, Pa
        Axis(minSensorK2,      maxSensorK2),      // sensor K2
        Axis(minSensorK3,      maxSensorK3)       // sensor K3, Pa
    )

    // Reference sensor
    const val sensorK1 = 130.0
    const val sensorK2 = -145.0
    const val sensorK3 = -125.0
    const val normalize = 3000.0

    // Sensor evaluated limits
    //const val sensorMinSpeed = 10
    //const val sensorMaxSpeed = 50
    const val sensorMinSpeed = 30.0
    const val sensorMaxSpeed = 70.0

    // Default wind
    const val windSpeed = 15.0
    const val windDirection = 180.0

    // Frequency
    const val frecuency = 20.0 // samples per second

    // default number of samples
    const val samples = 2000

    // Atmosphere conditions
    private const val pressure = 90060.0 // Pa
    private const val temperature = 20.0 // degC
    val environment = Environment(pressure, temperature)

    // Default aircraft speed function
    const val speedCycles = 1.5 // number of cycles
    const val speedBase = 30.0 // continuous part of speed, m/s
    const val speedDeviation = 9.0 // max deviation of speed, m/s

    // Default heading provider
    const val turns = 2.0 // turns over horizontal plane
}