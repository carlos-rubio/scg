/*
    SCG - Synthetic cases generator for sensitivity analyses
    Copyright (C) 2020 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package net.xnor.scg.synthetic.cases

import net.xnor.scg.model.Sensor
import net.xnor.scg.pso.Axis
import net.xnor.scg.synthetic.Environment
import net.xnor.scg.synthetic.Parameters
import net.xnor.scg.synthetic.providers.HeadingProvider
import net.xnor.scg.synthetic.providers.SpeedProvider
import net.xnor.scg.synthetic.providers.WindProvider

/**
 * Simulation case base interface
 */
interface Case {

    val dimensions: Int

    val axes: List<Axis>

    val discretizacion : Discretizacion

    val environment: Environment

    val sensorModel : Sensor

    val speedProvider: SpeedProvider

    val headingProvider: HeadingProvider

    val windProvider: WindProvider

    val parameters : Parameters

}